import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lista-fija',
  templateUrl: './lista-fija.component.html',
  styleUrls: ['./lista-fija.component.css']
})
export class ListaFijaComponent implements OnInit {

  @Input() elemento:string;
  constructor() { }

  ngOnInit(): void {
  }

}
