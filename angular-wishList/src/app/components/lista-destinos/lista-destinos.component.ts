import { Component, OnInit, Output, EventEmitter, ɵALLOW_MULTIPLE_PLATFORMS } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [
    DestinosApiClient
  ]
})
export class ListaDestinosComponent implements OnInit {
  lstFavoritos: string[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  all;

  constructor(public destinosApiClient: DestinosApiClient, public store: Store<AppState>) {
    // this.destinos = [];
    this.lstFavoritos = [];
    this.onItemAdded = new EventEmitter();
    

    //ngrx
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.lstFavoritos.push(`Has añadido ${d.nombre} a tus favoritos!`);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);

    //rxjs
    // this.destinosApiClient.suscribeOnChange((d: DestinoViaje) => {
    //   if (d != null) {
    //     this.lstFavoritos.push(`Has añadido ${d.nombre} a tus favoritos!`);
    //   }
    // });
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje) {
    this.destinosApiClient.elegir(d);
  }

  getAll(){

  }
}
