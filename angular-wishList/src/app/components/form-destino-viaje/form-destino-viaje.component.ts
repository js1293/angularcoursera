import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms'
import { fromEvent } from 'rxjs';
import { map,filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLenName: number = 3;
  maxLenName: number = 20;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(()=> APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.ValidateMinLength,
        this.ValidateMaxLength(this.maxLenName)
      ])],
      url: ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario:' + form);
    })
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e:KeyboardEvent)=> (e.target as HTMLInputElement).value),
      filter(text => text.length >2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  ValidateMinLength(control: FormControl): { [s: string]: boolean } {
    let len = control.value.toString().trim().length;
    if (len > 0 && len < 3) {
      return { invalidMinLength: true };
    }
    return null;
  }

  ValidateMaxLength(maxLen: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean | null } => {
      let len = control.value.toString().trim().length;
      if (len > maxLen) {
        return { invalidMaxLength: true };
      }
      return null;
    }

  }

}
