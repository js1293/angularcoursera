import { serializeNodes } from '@angular/compiler/src/i18n/digest';
import { ObjectUnsubscribedError } from 'rxjs';
import {v4 as uuid} from 'uuid';

export class DestinoViaje
{
    private selected : boolean;
    public servicios: string[];
    id = uuid();
    constructor(public nombre:string, public imageUrl:string, public votes:number = 0) {
        this.servicios = ['Piscina', 'Desayuno', 'Barra libre', "Mascota"];
    }

    isSelected() : boolean {
        return this.selected;
    }

    setSelected(s : boolean) : void {
        this.selected = s;   
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }

    reset(){
        this.votes = 0;
    }
}