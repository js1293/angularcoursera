describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
      cy.visit('http://localhost:4200');
      cy.contains('angular-wishList');
      cy.get('h1 b').should('contain', 'HOLA es');
    });

    it('tiene barra de navegacion', () =>{
        cy.visit('http://localhost:4200');
        cy.get('#navLinksId').should('have.class', 'navLinks')
    });
    it('boton de inicio guardar oculto al inicio', () =>{
        cy.visit('http://localhost:4200');
        cy.get('.btnForTest').should('not.be.visible')
    });
    
    
  });